const express = require('express')
const app = express()
const path = require('path')

app.use(express.static(path.join(__dirname, 'build')))
app.get('/', function (req, res) {
  res.sendFile('index.html', {root: './build' })
})

app.listen(3001, function () {
  console.log('Example app listening on port 3001!')
})