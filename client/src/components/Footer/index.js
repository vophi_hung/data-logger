import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <footer className="app-footer" style={{ minHeight: 120 }}>
         <span style={{ float: 'left', marginLeft: 12 }}>
          <img src={ '/img/lesco-logo.png' }  style={{
            marginBottom: 12,
            width: 170
          }}/>
         </span>
         
        <div style={{ float: 'left', width: 460, lineHeight: '22px', marginLeft: 24, marginBottom: 12 }}>
         CÔNG TY CỔ PHẦN GIẢI PHÁP MÔI TRƯỜNG SỐNG VIỆT NAM <br />
          Tel: (08) 35161279 - Fax: (08) 35161279<br />
          Email: lesco@lesco.com.vn<br />
          Website: www.lesco.com.vn<br />
          Address: 332F Phan Văn Trị , Phường 11, Quận Bình Thạnh, Tp Hồ Chí Minh<br />
        </div>
      </footer>
    )
  }
}
