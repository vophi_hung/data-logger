import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

export default class Sidebar extends Component {

  handleClick(e) {
    e.preventDefault()
    e.target.parentElement.classList.toggle('open')
  }

  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown'
  }

  // secondLevelActive(routeName) {
  //   return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse"
  // }

  render() {
    return (
      <div className="sidebar ">
        <nav className="sidebar-nav">
          <ul className="nav nav-list">
            <li className="nav-item">
              <NavLink to={'/dashboard'} className="nav-link" activeClassName="active">Trang chủ</NavLink>
            </li>
            <li className={this.activeRoute('water')}>
              <a className="nav-link nav-dropdown-toggle" href="#" onClick={this.handleClick.bind(this)}><i className="icon-drop" /> Trạm nước</a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/station/NUOC_DH3--slave'} className="nav-link" activeClassName="active">Duyên Hải 3 - Trạm nước 1</NavLink>
                </li>
              </ul>
            </li>
            <li className={this.activeRoute('air')}>
              <a className="nav-link nav-dropdown-toggle" href="#" onClick={this.handleClick.bind(this)}><i className="icon-speedometer" /> Trạm khí</a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/station/DH3_1_air--slave'} className="nav-link" activeClassName="active">Duyên Hải 3 - Trạm khí 1</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to={'/station/DH3_2_air--slave'} className="nav-link" activeClassName="active">Duyên Hải 3 - Trạm khí 2</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item">
              <NavLink to={'/report'} className="nav-link" activeClassName="active"><i className="icon-graph" /> Báo cáo </NavLink>
            </li>
            <li className={this.activeRoute('map')}>
              <a className="nav-link nav-dropdown-toggle" href="#" onClick={this.handleClick.bind(this)}><i className="icon-map" /> Bản đồ</a>
              <ul className="nav-dropdown-items">
                <li className="nav-item">
                  <NavLink to={'/map/water'} className="nav-link" activeClassName="active">Trạm nước</NavLink>
                  <NavLink to={'/map/air'} className="nav-link" activeClassName="active">Trạm khí</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}
