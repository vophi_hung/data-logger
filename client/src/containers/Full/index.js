import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

import { Header, Sidebar, Breadcrumb, Footer } from '../../components'
import { Dashboard, MapView, Report, Station } from '../../views/'

export default class Full extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  componentDidMount() {
    // if (!localStorageModule.isLogin()) {
    //   this.context.router.history.replace('/login')
    // }
  }

  render() {
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props} />
          <main className="main">
            {/*<Breadcrumb />*/}
            <div className="container-fluid">
                <Route path="/dashboard" name="Dashboard" component={Dashboard} />
                <Route path="/station/:name" name="Dashboard" component={Station} />                
                <Route path="/report" name="Dashboard" component={Report} />                
                <Route path="/map/:station" name="Dashboard" component={MapView} />                
                <Redirect from="/" to="/dashboard" />
              
            </div>
          </main>
        </div>
        <Footer />
      </div>
    )
  }
}
