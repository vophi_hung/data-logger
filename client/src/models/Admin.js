import Base, { Types } from './Base'

export default class Admin extends Base {

  static COLUMNS = {
    'id': {
      title: 'ID'
    },
    'active': {
      title: 'Active',
      type: Types.bool
    },
    'email': {
      title: 'Email'
    },
    'role': {
      title: 'Role',
      type: Types.enum(['none', 'admin', 'superadmin'])
    }
  }
}
