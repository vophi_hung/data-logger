import Base, { Types } from './Base'

export default class CallRate extends Base {

  static COLUMNS = {
    'id': {
      title: 'ID'
    },
    'active': {
      title: 'Active',
      type: Types.bool
    },
    'icon_url': {
      title: 'IconURL'
    },
    'is_default': {
      title: 'Active',
      type: Types.bool
    },
    'order': {
      title: 'Order',
      type: Types.number
    },
    'price': {
      title: 'Price',
      type: Types.number
    },
    'region_code': {
      title: 'Region code'
    },
    'title': {
      title: 'Title'
    },
    'subtitle': {
      title: 'Subtitle'
    },
    'type': {
      title: 'Type'
    }
  }
}
