import Base, { Types } from './Base'

export default class Product extends Base {

  static COLUMNS = {
    'id': {
      title: 'ID'
    },
    'product_id': {
      title: 'Product'
    },
    'used_seconds': {
      title: 'Used seconds',
      type: Types.number
    },
    'user_id': {
      title: 'User ID'
    },
    'active': {
      title: 'Active',
      type: Types.bool
    }
  }
}
