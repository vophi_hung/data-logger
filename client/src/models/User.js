import Base, { Types } from './Base'

export default class User extends Base {

  static COLUMNS = {
    'id': {
      title: 'ID'
    },
    'avatar': {
      title: 'Avatar'
    },
    'balance': {
      title: 'Balance',
      type: Types.number
    },
    'country_code': {
      title: 'CountryCode'
    },
    'fb_id': {
      title: 'FBID'
    },
    'name': {
      title: 'name'
    },
    'national_number': {
      title: 'NationalNumber'
    },
    'status': {
      title: 'Status',
      type: Types.enum([0, 1], { alias: ['normal', 'block'] })
    }
  }
}
