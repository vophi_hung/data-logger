import CrudService from './CrudService'
import { Admin } from '../../models'

export default class AdminService extends CrudService {
  constructor() {
    super(Admin)
    this.uri = 'admins'
    this.DEFAULT_PAGE = 'order=%5B%5B%22active%22%2C%22desc%22%5D%2C%5B%22role%22%2C%22desc%22%5D%2C%5B%22email%22%2C%22asc%22%5D%5D'
  }

  async login(accessToken) {
    const result = await this.post('/api/dashboard/login', {
      'access_token': accessToken
    })
    result.admin = new Admin(result.admin)
    return result
  }
}