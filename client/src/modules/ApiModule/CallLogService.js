import CrudService from './CrudService'
import { CallRate } from '../../models'

export default class CallLogService extends CrudService {
  constructor() {
    super(CallRate)
    this.uri = 'call-logs'
    this.DEFAULT_PAGE = 'order=%5B%5B%22date_created%22%2C%22desc%22%5D%2C%5B%22order%22%2C%22asc%22%5D%2C%5B%22title%22%2C%22asc%22%5D%5D'
  }

  //override
  async getList(page) {
    return await this.get(`/api/dashboard/${this.uri}?${page || this.DEFAULT_PAGE}`)
  }
}