import CrudService from './CrudService'
import { CallRate } from '../../models'

export default class CallRateService extends CrudService {
  constructor() {
    super(CallRate)
    this.uri = 'call-rates'
    this.DEFAULT_PAGE = 'order=%5B%5B%22active%22%2C%22desc%22%5D%2C%5B%22order%22%2C%22asc%22%5D%2C%5B%22title%22%2C%22asc%22%5D%5D'
  }
}