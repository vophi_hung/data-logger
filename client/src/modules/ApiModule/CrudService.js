import HttpService from './HttpService'

export default class CrudService extends HttpService {

  constructor(Model) {
    super()
    this.Model = Model
  }

  //API
  async create(item) {
    return await this.post(`/api/dashboard/${this.uri}`, item.toJSON())
  }

  async deleteItem(id) {
    console.log('go to delete service')
    return await this.delete(`/api/dashboard/${this.uri}/${id}`)
  }

  async getItem(id) {
    const object = await this.get(`/api/dashboard/${this.uri}/${id}`)
    return new this.Model(object)
  }

  async getList(page) {
    const result = await this.get(`/api/dashboard/${this.uri}?${page || this.DEFAULT_PAGE}`)
    result.objects = result.objects.map(object => new this.Model(object))
    return result
  }

  async update(item) {
    return await this.put(`/api/dashboard/${this.uri}`, item.toJSON())
  }
}