import CrudService from './CrudService'
import { CurrencyRate } from '../../models'

export default class CurrencyRateService extends CrudService {
  constructor() {
    super(CurrencyRate)
    this.uri = 'currency-rates'
    this.DEFAULT_PAGE = 'order=%5B%5B%22active%22%2C%22desc%22%5D%2C%5B%22order%22%2C%22asc%22%5D%2C%5B%code%22%2C%22asc%22%5D%5D'
  }
}