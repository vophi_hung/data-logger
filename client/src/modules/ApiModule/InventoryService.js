import CrudService from './CrudService'
import { Inventory } from '../../models'

export default class InventoryService extends CrudService {
  constructor() {
    super(Inventory)
    this.uri = 'inventories'
    this.DEFAULT_PAGE = 'active%22%2C%22desc%22%5D%2C%5B%22order%22%2C%22asc%22%5D%2C%5B%22title%22%2C%22asc%22%5D%5D'
  }
}