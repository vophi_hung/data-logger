import CrudService from './CrudService'
import { Product } from '../../models'

export default class ProductService extends CrudService {
  constructor() {
    super(Product)
    this.uri = 'products'
    this.DEFAULT_PAGE = 'order=%5B%5B%22active%22%2C%22desc%22%5D%2C%5B%22order%22%2C%22asc%22%5D%2C%5B%22title%22%2C%22asc%22%5D%5D'
  }
}