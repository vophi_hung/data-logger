import CrudService from './CrudService'
import { User } from '../../models'

export default class UserService extends CrudService {
  constructor() {
    super(User)
    this.uri = 'users'
    this.DEFAULT_PAGE = 'order=%5B%5B%22status%22%2C%22desc%22%5D%2C%5B%22updated_at%22%2C%22desc%22%5D%5D'
  }
}