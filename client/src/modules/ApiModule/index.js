import AdminService from './AdminService'
import CallLogService from './CallLogService'
import CallRateService from './CallRateService'
import CurrencyRateService from './CurrencyRateService'
import InventoryService from './InventoryService'
import ProductService from './ProductService'
import UserService from './UserService'

export const
  adminService = new AdminService(),
  callLogService = new CallLogService(),
  callRateService = new CallRateService(),
  currencyRateService = new CurrencyRateService(),
  inventoryService = new InventoryService(),
  productService = new ProductService(),
  userService = new UserService()
