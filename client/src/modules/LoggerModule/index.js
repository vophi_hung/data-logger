/* eslint no-console: ["error", { allow: ["debug", "log", "warn"] }] */

export default class Logger {

  constructor(options = {}) {
    const { prefix = 'CallHome' } = options
    this._prefix = prefix
  }

  initialize({ configModule }) {
    this._enabled = configModule.isDebug()
  }

  debug() {
    if (this._enabled) {
      console.debug(this._prefix, ...arguments)
    }
  }

  log() {
    if (this._enabled) {
      console.log(this._prefix, ...arguments)
    }
  }

  setEnabled(enabled) {
    this._enabled = enabled
  }

  warn() {
    if (this._enabled) {
      console.warn(this._prefix, ...arguments)
    }
  }
}
