import ConfigModule from './ConfigModule'
import LocalStorageModule from './LocalStorageModule'
import LoggerModule from './LoggerModule'

export const
  configModule = new ConfigModule(),
  localStorageModule = new LocalStorageModule(),
  loggerModule = new LoggerModule()
  