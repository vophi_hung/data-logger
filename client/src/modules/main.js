import * as modules from './index'
Object.values(modules).filter(module => !!module.initialize).forEach(module => module.initialize(modules))
export * from './index'
