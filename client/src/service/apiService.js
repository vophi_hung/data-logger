import { httpService } from './'

export default class ApiService {

  getStartupData(name, sensor, limit=10) {
    return httpService.get(`/data/startup/${name}/${sensor}?limit=${limit}`)    
  }

  getListStation(name) {
    return httpService.get(`/data/station/${name}`)
  }

  getListSensor(name) {
    return httpService.get(`/data/sensor/${name}`)
  }

  getReport(name, start, end) {
    return httpService.get(`/data/sensor/report/${name}?start=${start}&end=${end}`)    
  }

  getLatest(name) {
    return httpService.get(`/data/sensor/latest/${name}`)    
  }
}