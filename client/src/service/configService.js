import config from '../config/config'

export default class ConfigModule {

  getApiEndpoint() {
    return config.apiEndpoint
  }

  getEnv() {
    return config.env
  }

  getGoogleClientId() {
    return config.googleClientId
  }

  isDebug() {
    return !!config.debug
  }
}
