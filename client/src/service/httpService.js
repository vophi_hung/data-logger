import {
  localStorageService
} from './'
import { configService } from './'
import * as utils from './utilService'


export default class HttpService {

  buildHeaders(headers) {
    return Object.assign({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorageService.getToken()}`
    }, headers || {})
  }

  delete(path, query, headers) {
    return this
      .request({
        method: 'DELETE',
        path,
        query,
        headers
      })
      .then(res => {
        return utils.idx(res, _ => _.results.object)
      })
  }

  get(path, query, headers) {
    return this
      .request({
        method: 'GET',
        path,
        query,
        headers
      })
      .then(res => {
        const object = utils.idx(res, res => res.results.object)
        const objects = utils.idx(res, res => res.results.objects)
        const currentPage = utils.idx(res, res => res.pagination['current_page'])
        const nextPage = utils.idx(res, res => res.pagination['next_page'])
        const prevPage = utils.idx(res, res => res.pagination['prev_page'])
        if (object) {
          return object
        }
        if (objects) {
          return {
            currentPage,
            nextPage,
            objects,
            prevPage
          }
        }
        return null
      })
  }

  post(path, body, headers) {
    return this
      .request({
        method: 'POST',
        path,
        body,
        headers: this.buildHeaders(headers)
      })
      .then(res => {
        return utils.idx(res, _ => _.results.object)
      })
  }

  put(path, body, headers) {
    return this
      .request({
        method: 'PUT',
        path,
        body,
        headers
      })
      .then(res => {
        return utils.idx(res, _ => _.results.object)
      })
  }

  request({
    method,
    path,
    query = {},
    headers = {},
    body = {}
  }) {
    const API_ENDPOINT = configService.getApiEndpoint()
    headers = Object.assign({}, this.buildHeaders(), headers || {})
    const url = /^(http|https):\/\//.test(path) ? path : utils.buildUrl(API_ENDPOINT, {
      path,
      query
    })
    const payload = {
      method: method,
      headers: this.buildHeaders(headers)
    }
    if (method !== 'GET' && method !== 'HEAD') {
      payload.body = JSON.stringify(body)
    }
    return fetch(url, payload)
      .then(res => {
        if (res.status !== 200) {
          return res
            .json()
            .catch(() => {
              return {
                code: res.status,
                message: res['_bodyText']
              }
            })
            .then(error => {
              switch (error.code) {
                case 401:
                  localStorageService.setToken(null)
                  utils.reload()
                  break
                default:
                  break
              }
              throw new Error(error.message || 'Unknown error.')
            })
        }
        return res
      })
      .then(res => utils.idx(res, _ => _.json().catch(() => null)))
      .then(data => {
        return data
      })
  }
}