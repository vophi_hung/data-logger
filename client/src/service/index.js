import ApiService from './apiService'
import ConfigService from './configService'
import HttpService from './httpService'
import LocalStorageService from './localStorageService'

const apiService = new ApiService()
const configService = new ConfigService()
const httpService = new HttpService()
const localStorageService = new LocalStorageService()

export {
  apiService,
  configService,
  httpService,
  localStorageService,
}
