const ADMIN = 'admin'
const TOKEN = 'token'

export default class LocalStorageService {

  getAdmin() {
    return new this._get(ADMIN)
  }

  getToken() {
    return this._get(TOKEN)
  }

  isLogin() {
    return this._contain(TOKEN)
  }

  setAdmin(admin) {
    this._set(ADMIN, admin.toJSON())
  }

  setToken(token) {
    this._set(TOKEN, token)
  }

  _contain(key) {
    return !!this._get(key)
  }

  _del(key) {
    localStorage.removeItem(key)
  }

  _get(key) {
    let data = localStorage.getItem(key)
    try {
      return JSON.parse(data)
    } catch (error) {
      return data
    }
  }

  _set(key, data) {
    localStorage.setItem(key, data)
  }
}
