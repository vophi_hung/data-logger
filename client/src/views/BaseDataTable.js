import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Form, PaginatedTable } from '../../components'
import { Product } from '../../models'
import { apiModule } from '../../modules'
import * as utils from '../../utils'

const DETAIL_COLUMNS = [{
  name: 'gp_product_code'
}, {
  name: 'regions_code'
}, {
  name: 'title'
}, {
  name: 'description'
}, {
  name: 'icon_url'
}, {
  name: 'seconds'
}, {
  name: 'expire_at'
}, {
  name: 'expire_in'
}, {
  name: 'price'
}, {
  name: 'visible'
}, {
  name: 'active'
}, {
  name: 'order'
}]

const LIST_COLUMNS = [{
  name: 'id',
  style: {
    maxWidth: '80px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
  }
}, {
  name: 'gp_product_code',
  style: {
    maxWidth: '80px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
  }
}, {
  name: 'regions_code'
}, {
  name: 'title'
}, {
  name: 'seconds'
}, {
  name: 'expire_at'
}, {
  name: 'expire_in'
}, {
  name: 'price'
}, {
  name: 'visible'
}, {
  name: 'active'
}, {
  name: 'order'
}]

export default class Products extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    currentPage: null,
    nextPage: null,
    objects: [],
    prevPage: null
  }

  componentDidMount() {
    this._loadProducts()
  }

  render() {
    const { objects } = this.state
    return (
      <div className="animated fadeIn">
        <PaginatedTable ref="table"
          columns={LIST_COLUMNS}
          data={objects}
          model={Product}
          page={this.state.currentPage}
          renderCreateLayout={this._renderCreateLayout}
          renderEditLayout={this._renderEditLayout}
          onNextPageClick={this._onNextPageClick}
          onOrderClick={this._onOrderClick}
          onPageSizeClick={this._onPageSizeClick}
          onPrevPageClick={this._onPrevPageClick}
          onSearchClick={this._onSearchClick} />
      </div>
    )
  }

  _loadProducts = async (page = this.state.currentPage) => {
    const { currentPage, nextPage, objects, prevPage } = await apiModule.getProducts(page)
    this.setState({ currentPage, nextPage, objects, prevPage })
  }

  _onCreateSubmit = async (data) => {
    const product = new Product(data)
    await apiModule.createProduct(product).catch(error => {
      throw error
    })
    await this._loadProducts()
    if (this.refs.createForm) {
      this.refs.createForm.reset()
    }
  }

  _onEditSubmit = async (data) => {
    const product = new Product(data)
    await apiModule.updateProduct(product).catch(error => {
      throw error
    })
    await this._loadProducts()
    const newProduct = await apiModule.getProduct(data.id)
    if (this.refs.editForm) {
      this.refs.editForm.reset(newProduct)
    }
  }

  _onNextPageClick = () => {
    if (this.state.nextPage) {
      this._loadProducts(this.state.nextPage)
    }
  }

  _onOrderClick = (text) => {
    this._loadProducts(utils.buildQuery(this.state.currentPage, {
      'order': text
    }))
  }

  _onPageSizeClick = (text) => {
    this._loadProducts(utils.buildQuery(this.state.currentPage, {
      'page_size': text
    }))
  }

  _onPrevPageClick = () => {
    if (this.state.prevPage) {
      this._loadProducts(this.state.prevPage)
    }
  }

  _onSearchClick = (text) => {
    this._loadProducts(utils.buildQuery(this.state.currentPage, {
      'filter': text
    }))
  }

  _renderCreateLayout = () => {
    return (
      <Form ref={(target) => this.refs.createForm = target}
        columns={DETAIL_COLUMNS}
        model={Product}
        title="Create Product"
        onSubmit={this._onCreateSubmit} />
    )
  }

  _renderEditLayout = (object) => {
    return (
      <Form ref={(target) => this.refs.editForm = target}
        columns={DETAIL_COLUMNS}
        data={object}
        model={Product}
        title={`Edit Product - <${object.id}>`}
        onSubmit={this._onEditSubmit} />
    )
  }
}
