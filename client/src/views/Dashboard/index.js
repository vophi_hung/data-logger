﻿import React, { Component } from 'react'
import DashboardBlock from '../components/infoBlock/dashboardBlock'



class Dashboard extends Component {

  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      dropdownOpen: false
    }
    
    setInterval(() => {
      this.setState({
        divKey: new Date().getTime()
      })
    }, 5*60*1000)
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  componentDidMount() {
    
  }



  render() {
    console.log('re render dashboard ==========================', (new Date()))
    return (
      <div className="animated fadeIn"  key={this.state.divKey}>
        <div className="row">
          <div className="col-md-6" style={{ border: '1px solid #aaa'}}>
            
            <h3 className="card-title" style={{ margin: 12 }}>Trạm khí 1</h3>
            <div className="col-md-4 pull-left">
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'FLOW'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'DUST'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'T'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'H'} />             
            </div>
            <div className="col-md-4 pull-left">
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'CO'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'NO'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'H2O'} />      
            </div>
            
            <div className="col-md-4 pull-left">
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'CO2'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'SO2'} />             
              <DashboardBlock name={'DH3_1_air--slave'} sensor={'O2'} />      
            </div>
          </div> 
          <div className="col-md-6" style={{ border: '1px solid #aaa'}}>
            
            <h3 className="card-title" style={{ margin: 12 }}>Trạm khí 2</h3>
            <div className="col-md-4 pull-left">
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'FLOW'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'DUST'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'T'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'H'} />             
            </div>
            <div className="col-md-4 pull-left">
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'CO'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'NO'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'H2O'} />      
            </div>
            
            <div className="col-md-4 pull-left">
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'CO2'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'SO2'} />             
              <DashboardBlock name={'DH3_2_air--slave'} sensor={'O2'} />      
            </div>
          </div> 
          <div className="col-md-6" style={{ border: '1px solid #aaa'}}>
            
            <h3 className="card-title" style={{ margin: 12 }}>Trạm nước 1</h3>
            <div className="col-md-3 pull-left">
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'COD'} />             
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'CLO'} />           
            </div>
            <div className="col-md-3 pull-left">
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'TSS'} />             
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'FLOW2'} />           
            </div>
            <div className="col-md-3 pull-left">
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'PH'} />             
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'TP'} />           
            </div>
            <div className="col-md-3 pull-left">
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'T'} />             
              <DashboardBlock name={'NUOC_DH3--slave'} sensor={'TN'} />           
            </div>
          </div> 
          <div className="col-md-6">
            Camera IP
          </div>
          {/*Add color :D*/}
          
        </div>
        
      </div>
    )
  }
}

export default Dashboard
