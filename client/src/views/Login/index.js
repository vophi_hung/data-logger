import React, { Component } from 'react'
import GoogleLogin from 'react-google-login'
import PropTypes from 'prop-types'
import { adminService } from '../../modules/ApiModule'
import { configModule, localStorageModule } from '../../modules'

const BG_COLOR = 'rgb(242, 111, 16)'

export default class Login extends Component {

  static contextTypes = {
    router: PropTypes.object
  }

  state = {
    googleClientId: configModule.getGoogleClientId()
  }

  componentDidMount() {
    if (localStorageModule.isLogin()) {
      this.context.router.history.replace('/')
    }
  }

  render() {
    let { googleClientId } = this.state
    return (
      <div className="app flex-row align-items-center">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-8">
              <div className="card-group mb-0">
                <div style={{ borderWidth: 0 }} className="card p-4">
                  <div className="card-block">
                    <h1>Login</h1>
                    <p>Login to your MySQUAR Google account</p>
                    <div className="input-group mb-3">
                      <GoogleLogin className="btn btn-lg btn-google-plus" type="button"
                        clientId={googleClientId}
                        onSuccess={this._responseGoogle}
                        onFailure={this._responseGoogle}>
                        <span>account@mysquar.com</span>
                      </GoogleLogin>
                    </div>
                  </div>
                </div>
                <div className="card card-inverse card-primary py-5 d-md-down-none" style={{ width: 44 + '%', backgroundColor: BG_COLOR, border: 'none' }}>
                  <div className="card-block text-center">
                    <div>
                      <img className="img-responsive" alt="" src="http://callhome.mysquar.com/img/callhome-logo.png" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  _responseGoogle = async (data) => {
    const { id_token: accessToken } = data.getAuthResponse()
    let { admin, token } = await adminService.login(accessToken)
    localStorageModule.setAdmin(admin)
    localStorageModule.setToken(token)
    this.context.router.history.replace('/')
  }
}
