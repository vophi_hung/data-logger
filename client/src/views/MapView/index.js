import React, { Component } from 'react'
import { apiService } from '../../service'
import {
  withGoogleMap,
  GoogleMap,
  InfoWindow,
  Marker
} from 'react-google-maps'
import moment from 'moment'
import { getName, getUnit } from '../utils'


const googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.27&libraries=places,geometry&key=YOUR_GOOGLE_MAPS_API_KEY_GOES_HERE"

const PopUpInfoWindowExampleGoogleMap = withGoogleMap(props => (
  <GoogleMap
    defaultZoom={14}
    center={props.center}
    googleMapURL={googleMapURL}
  >
    {props.markers.map((marker, index) => (
      <Marker
        key={index}
        position={marker.position}
        onClick={() => props.onMarkerClick(marker)}
      >
        {marker.showInfo && (
          <InfoWindow onCloseClick={() => props.onMarkerClose(marker)}>
            <div>{marker.infoContent}</div>
          </InfoWindow>
        )}
      </Marker>
    ))}
  </GoogleMap>
))

const stations = {
  water: [
    {
      code: 'NUOC_DH3--slave',
      name: 'Trạm nước 1',
      position: {
        lat: 9.599297,
        lng: 106.5185413
      }
    }
  ],
  air: [
    {
      code: 'DH3_1_air--slave',
      name: 'Trạm khí 1',
      position: {
        lat: 9.599297,
        lng: 106.5185413
      }
    },
    {
      code: 'DH3_2_air--slave',
      name: 'Trạm khí 2',
      position: {
        lat: 9.599297,
        lng: 106.5285413
      }
    }
  ]
}

class MapView extends Component {
  state = {
    center: {
      lat: 9.599297,
      lng: 106.5185413
    },

    // array of objects of markers
    markers: [
      
    ],
    shown: [

    ]
  };

  renderData(data, name) {
    return (<table className="table table-bordered">
      <tbody>
        <tr>
          <td colSpan={2}>{name}</td>
        </tr>
        {
          data && Object.keys(data).map(key => {
            return <tr>
              <td  dangerouslySetInnerHTML={{ __html: getName(key)}}></td>
              <td  dangerouslySetInnerHTML={{ __html: `${(key === 'timeStamp')? moment(data[key]).format('DD-MM-YYYY hh:mm'): Math.round(data[key] * 100) / 100} ${getUnit(key)}`}}></td>
            </tr>
          })
        }
        
      </tbody>
    </table>)
  }

  getData = this.getData.bind(this)
  async getData(name, position, stationName) {
    const data = await apiService.getLatest(name)
    const { markers } = this.state
    markers.push({
      position,
      showInfo: true,
      infoContent: this.renderData(data, stationName),
      stationName: stationName,

      data
    })
    this.setState({
      markers      
    })
  }

  handleMarkerClick = this.handleMarkerClick.bind(this);
  handleMarkerClose = this.handleMarkerClose.bind(this);
  uncheckChatbox = this.uncheckChatbox.bind(this);

  uncheckChatbox() {
    Object.keys(this.refs).forEach(key => {
      this.refs[key].checked = false
    })
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.setState({
        markers: []
      })
      this.uncheckChatbox()
    }
  }

  // Toggle to 'true' to show InfoWindow and re-renders component
  handleMarkerClick(targetMarker) {
    this.setState({
      markers: this.state.markers.map(marker => {
        if (marker === targetMarker) {
          return {
            ...marker,
            showInfo: true
          }
        }
        return marker
      })
    })
  }

  handleMarkerClose(targetMarker) {
    this.setState({
      markers: this.state.markers.map(marker => {
        if (marker === targetMarker) {
          return {
            ...marker,
            showInfo: false
          }
        }
        return marker
      })
    })
  }

  handleInputChange = this.handleInputChange.bind(this)

  handleInputChange(event, position, stationName) {
    let { markers } = this.state
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    if (value) {
      this.getData(name, position, stationName)
    } else {
      markers = markers.filter(marker => {
        return marker.stationName !== stationName
      })
      this.setState({
        markers
      })
    }

    
  }

  render() {
    const { station } = this.props.match.params
    const sts = stations[station]
    const { data, markers } = this.state
    return (
      <div className="animated fadeIn" style={{ marginTop: 20 }}>
        <div className="col-md-3 float-left" >
          <div className="card">
            <div className="card-block" style={{ height: 'auto'}}>
              {
                sts.map(({ code, name, position }) => {
                  return (<div className="checkbox" keys={code}>
                    <label htmlFor="checkbox1">
                      
                      <input ref={code} type="checkbox" id="checkbox1" value="option1"
                        name={code}
                        defaultChecked={false}
                        onChange={(e) => this.handleInputChange(e, position, name)}/> {name}
                    </label>
                  </div>)
                })
              }  
              
            </div>
          </div>
          {
            markers.map(marker => {
              return (<div className="card">
                <div className="card-block" style={{ height: 'auto'}}>
                  {
                    this.renderData(marker.data, marker.stationName)
                  }
                </div>
              </div>)
            })
          }
        </div>
        <div className="col-md-9 float-left" >
          <div className="card">
            <div className="card-block" style={{ height: '480px'}}>
              <PopUpInfoWindowExampleGoogleMap
                containerElement={
                  <div style={{ height: '100%' }} />
                }
                mapElement={
                  <div style={{ height: '100%' }} />
                }
                center={this.state.center}
                markers={this.state.markers}
                onMarkerClick={this.handleMarkerClick}
                onMarkerClose={this.handleMarkerClose}
                
                data={data}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default MapView
