import React, { Component } from 'react'
import 'react-infinite-calendar/styles.css'
import fileDownload from 'react-file-download'
import XLSX from 'xlsx'
import moment from 'moment'
import { apiService } from '../../service'
import { getName, getUnit } from '../utils'

class Report extends Component {

  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      dropdownOpen: false,
      name: 'NUOC_DH3--slave'
    }

    this.getData = this.getData.bind(this)
  }

  downloadFile = this.downloadFile.bind(this)
  async downloadFile() {
    const { data } = this.state
    const wopts = { bookType:'xlsx', bookSST:false, type:'binary' }
    
    const wb = {
      Sheets: {
        report: XLSX.utils.json_to_sheet(data)
      },
      SheetNames: ['report']
    }

    var wbout = XLSX.write(wb, wopts)

    function s2ab(s) {
      var buf = new ArrayBuffer(s.length)
      var view = new Uint8Array(buf)
      for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF
      return buf
    }
    

    fileDownload(new Blob([s2ab(wbout)],{type: 'application/octet-stream' }), 'report.xlsx')
  }

  async getData() {
    const { name, start, end } = this.state
    console.log(name, start, end)
    this.setState({
      sensors: [],
      data: []
    })
    
    this.setState({
      ... await apiService.getReport(name, start, end)
    })
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  render() {
    const { sensors = [], data = []} = this.state
    return (
      <div className="animated fadeIn">
        <div className="col-xs-12">
          <div className="card">
            <div className="card-block">
              <div className="row">
                <div className="form-group col-sm-3">
                  <label htmlFor="ccmonth">Trạm</label>
                  <select className="form-control" id="name"  onChange={e => this.setState({ name: e.target.value })}>
                    <option value={'NUOC_DH3--slave'}>Duyên Hải 3 - Trạm nước 1</option>
                    <option value={'DH3_1_air--slave'}>Duyên Hải 3 - Trạm khí 1</option>
                    <option value={'DH3_2_air--slave'}>Duyên Hải 3 - Trạm khí 2</option>
                  </select>
                </div>
                <div className="form-group col-sm-3">
                  <label htmlFor="ccmonth">Bắt đầu</label>
                  <input className="form-control" type="date" defaultValue={new Date()} onChange={e => this.setState({ start: e.target.value })} />
                </div>
                <div className="form-group col-sm-3">
                  <label htmlFor="ccmonth">Kết thúc</label>
                  <input className="form-control" type="date" defaultValue={new Date()}  onChange={e => this.setState({ end: e.target.value })}/>
                </div>
                <div className="form-group col-sm-3 text-center" style={{marginTop: 30}}>
                  <button type="button" className="btn btn-primary" onClick={() => this.getData()}>Xem</button>
                  <button type="button" className="btn btn-secondary" onClick={() => this.downloadFile()}>Xuất file</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="card">
            <div className="card-block">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Thời gian</th>
                    {sensors.map(sensor => {
                      return <th  dangerouslySetInnerHTML={{ __html: `${getName(sensor)}<br />${getUnit(sensor)}` }} ></th>
                    })}
                  </tr>
                </thead>
                <tbody>
                  {
                    data.map(row => {
                      return <tr>
                        <td>{moment(row.timeStamp).format('DD-MM-YYYY hh:mm')}</td>
                        {
                          sensors.map(sensor => {
                            return <td>{
                              row[sensor]
                              ? Math.round(row[sensor] * 100) / 100
                              : 0}</td>
                          })
                        }
                      </tr>
                    })
                  }
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Report
