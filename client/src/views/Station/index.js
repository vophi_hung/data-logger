import React, { Component } from 'react'
// import io from 'socket.io-client'
import InfoBlock from '../components/infoBlock'
import GraphBlock from '../components/graphBlock'
import { apiService } from '../../service'
// const socket = io('http://localhost:3000')

const unit = {
  so2: 'mg/Nm3', 
  no: 'mg/Nm3', 
  dust: 'mg/Nm3', 
  co: 'mg/Nm3', 
  o2: '%', 
  h20: '%', 
  co2: '%', 
  tss: 'mg/l',
  ph: '',
  cod: 'mg/l',
  flow: 'm3/h',
  t: '<sup>o</sup>C',
  temperature: '<super>o</super>C',
  pressure: 'Pa',
  tn: 'mg/l',
  p: 'Pa'
}

const sensorsList = {
  air: [
    'FLOW',
    'CO',
    'CO2',
    'DUST',
    'NO',
    'SO2',
    'T',
    'H2O',
    'O2',
    'H'
  ],
  water: [
    'COD',
    'TSS',
    'PH',
    'T',
    'TN',
    'CLO',
    'FLOW2',
    'TP'
  ]
}

const stationName = {
  air: [
    'DH3_1_air--slave',
    'DH3_2_air--slave'
  ],
  water: [
    'DH3_2_air--slave'
  ]
}

class Dashboard extends Component {

  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      dropdownOpen: false
    }

    this.getData = this.getData.bind(this)
    this.getData()
  }

  componentDidUpdate(prevProps) {
    // console.log('did mount ======================')
    // console.log(this.props)
    if (this.props.location !== prevProps.location) {
      this.setState({
        name: '',
        sensors: []
      })
    }
  }

  async getData() {
    if(this.refs.data) {
      // console.log('get data =======================')
      // this.setState({
      //   sensors: [],
      //   name: ''
      // })
      

      // console.log(sensors)
      // console.log(name)

      // this.setState({
      //   sensors,
      //   name
      // })
    }    
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  render() {
    console.log('re render ==========================')
    const { name } = this.props.match.params
    let sensors = []
    if(stationName.air.indexOf(name) > -1) {
      sensors = sensorsList['air']
    } else {
      sensors = sensorsList['water']
    }
    return (
      <div className="animated fadeIn" style={{ marginTop: 20 }}>
        <div ref="data" className="row">
          {/*Add color :D*/}
          {
            sensors && sensors.map(sensor => {
              return <InfoBlock name={name} sensor={sensor} key={'gr' + (new Date()).getTime() + sensor} /> 
            })
          }
        </div>
        <div className='row'>
          {
            sensors && sensors.map(sensor => {
              return <GraphBlock name={name} sensor={sensor} key={ (new Date()).getTime() + sensor} /> 
            })
          }
          
        </div>
        
      </div>
    )
  }
}

export default Dashboard
