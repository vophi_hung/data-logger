import React, { Component } from 'react'
import io from 'socket.io-client'
import { Line } from 'react-chartjs-2'
import { apiService } from '../../../service'
import moment from 'moment'
import { getName } from '../../utils'

const socket = io('http://localhost:3000')
const brandInfo = '#63c2de'
function convertHex(hex, opacity) {
  hex = hex.replace('#', '')
  var r = parseInt(hex.substring(0, 2), 16)
  var g = parseInt(hex.substring(2, 4), 16)
  var b = parseInt(hex.substring(4, 6), 16)

  var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')'
  return result
}

export default class GraphBlock extends Component {
  constructor(props) {
    super(props)
    const { sensor } = props
    this.state = {
      mainChart: {
        labels: [],
        datasets: [
          {
            label: sensor,
            backgroundColor: convertHex(brandInfo, 10),
            borderColor: brandInfo,
            pointHoverBackgroundColor: '#fff',
            borderWidth: 2,
            data: []
          }
        ]
      },
      mainChartOpts: {
        pointRadius: 1,
        pointStyle: 'circle',
        maintainAspectRatio: false,
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            gridLines: {
              drawOnChartArea: false
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
              maxTicksLimit: 5
            }
          }]
        },
        elements: {
          point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3
          }
        }
      }
    }
    this.getStartupData = this.getStartupData.bind(this)
  }

  async getStartupData() {
    let { mainChart } = this.state
    const { name, sensor } = this.props
    let { objects } = await apiService.getStartupData(name, sensor)
    objects = objects.reverse()
    objects.forEach(data => {
      const { value, timeStamp: timestamp } = data
      mainChart.labels.push(moment(timestamp).format('DD/MM HH:mm'))
      mainChart.datasets[0].data.push(Math.round(value * 100) / 100)
    })
    this.setState({ mainChart })
  }

  componentDidMount() {
    const { name, sensor } = this.props
    try {
      socket.on('connect', () => {
        socket.on(`${name}-${sensor}`, data => {
          const { value, timeStamp: timestamp } = data
          let { mainChart } = this.state
          mainChart.labels.push(moment(timestamp).format('DD/MM HH:mm'))
          mainChart.datasets[0].data.push(Math.round(value * 100) / 100)
          this.setState({ mainChart })
        })
      })
      this.getStartupData()
    } catch (error) {
    }    
  }

  render() {
    const { sensor } = this.props
    const { mainChart, mainChartOpts } = this.state
    return (
    <div className="col-xs-12 col-sm-12 col-md-6">
      <div className="card">
        <div className="card-block">
          <div className="row">
            <div className="col-sm-12 text-center">
              <h3 className="card-title mb-0"  dangerouslySetInnerHTML={{ __html: getName(sensor)}}></h3>
            </div>
          </div>
          <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
            <Line data={mainChart} options={mainChartOpts} height={300} />
          </div>
        </div>
      </div>
    </div>)
  }
}