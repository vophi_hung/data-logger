import React, { Component } from 'react'
import io from 'socket.io-client'
import { apiService } from '../../../service'
import moment from 'moment'
import { getName, getUnit } from '../../utils'

const socket = io('http://localhost:3000')

export default class InfoBlock extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: 0,
      timestamp: '',
      warning: 'N/A'
    }

    this.getStartupData = this.getStartupData.bind(this)
  }

  async getStartupData() {
    const { name, sensor } = this.props
    const { objects } = await apiService.getStartupData(name, sensor, 1)
    
    if (objects.length > 0 ) {
      const { value, timeStamp, quality } = objects[0]
      this.setState({
        value: Math.round(value * 100) / 100,
        timestamp: moment(timeStamp).format('DD/MM/YYYY HH:mm'),
        warning: quality
      })
    }
  }

  componentDidMount() {
    const { name, sensor } = this.props
    try {
      socket.on('connect', () => {
        socket.on(`${name}-${sensor}`, data => {
          const { value, timeStamp: timestamp, quality } = data
          this.setState({
            value: Math.round(value * 100) / 100,
            timestamp: moment(timestamp).format('DD/MM/YYYY HH:mm'),
            warning: quality
          })
        })
      })
      this.getStartupData()
    } catch (error) {
    }    
  }

  render() {
    const { sensor } = this.props
    const { value, timestamp, warning } = this.state
    return (
    <div className="col-sm-6 col-lg-3">
      <div className="card card-inverse card-primary" style={{ marginBottom: '0px'}}>
        <div className="card-block">
          <div className="btn-group float-right">
            <p style={{ textAlign: 'right', marginBottom: '0px'}}>
              <h4 className="mb-0">{ value }</h4> 
              <h6 className="mb-0" dangerouslySetInnerHTML={{ __html: getUnit(sensor)}}></h6>
            </p>
          </div>
          <h4 className="mb-0" dangerouslySetInnerHTML={{ __html: getName(sensor)}}></h4>
        </div>
      </div>
      <div className="card" style={{ marginBottom: '0px'}}>
        <div className="card-block" style={{ padding: '10px'}}>
          <div className="btn-group float-right">
            { timestamp }
          </div>
          <i className="icon-clock" />
        </div>
      </div>
      <div className="card">
        <div className="card-block" style={{ padding: '10px'}}>
          <div className="btn-group float-right">
            { warning }
          </div>
          <i className="icon-bell" />
        </div>
      </div>
    </div>)
  }
}