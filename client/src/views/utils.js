﻿const getName = sensor => {
  const name = {
    so2: 'SO<sub>2</sub>', 
    no: 'NO', 
    dust: 'Bụi tổng', 
    co: 'CO', 
    o2: 'O<sub>2</sub>', 
    h2o: 'H<sub>2</sub>O', 
    co2: 'CO<sub>2</sub>', 
    tss: 'TSS',
    ph: 'pH',
    cod: 'COD',
    flow: 'Lưu lượng',
    flow2: 'Lưu lượng',
    t: 'Nhiệt độ',
    h: 'Độ ẩm',
    temperature: 'Nhiệt độ',
    pressure: 'Áp suất',
    tn: 'Tổng Nito',
    tp: 'Tổng Photpho',
    p: 'Pa',
    timestamp: 'Thời gian',
    co21: 'O<sub>2</sub>',
    clo: 'Clo'
  }

  return name[sensor.toLowerCase(sensor)] || sensor
}

const getUnit = sensor => {
  const unit = {
    so2: 'mg/Nm<sup>3</sup>', 
    no: 'mg/Nm<sup>3</sup>', 
    dust: 'mg/Nm<sup>3</sup>', 
    co: 'mg/Nm<sup>3</sup>', 
    o2: '%',
    co21: '%',
    h2o: '%', 
    co2: '%', 
    tss: 'mg/l',
    ph: '&nbsp;',
    cod: 'mg/l',
    flow: 'm<sup>3</sup>/h',
    flow2: 'm<sup>3</sup>/h',
    t: '<sup>o</sup>C',
    temperature: '<sup>o</sup>C',
    pressure: 'Pa',
    tn: 'mg/l',
    tp: 'mg/l',
    h: '%',
    p: 'Pa',
    timestamp: ' ',
    clo: 'mg/l'
  }

  return unit[sensor.toLowerCase(sensor)] || ''
}

export {
  getName,
  getUnit
}