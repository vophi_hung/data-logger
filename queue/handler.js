const Queue = require('bull')
const Jobs = require('./jobs')
module.exports = {
  init: () => {
    Object.keys(Jobs).forEach(job => {
      const { concurrency, handler } = Jobs[job]
      
      console.log(`Init queue ${job} with ${concurrency} concurrency `);
      const queue = Queue(job, {
        redis: {
          host: 'localhost',
          port: 6379
        }
      })
      queue.process(concurrency, handler)
    })
  }
}