const {
  default: {
    Data
  }
} = require('../../server/dist/collections')
const fs = require('fs')
const path = require('path')
const io = require('socket.io-client')
const socket = io('http://localhost:3000')

const firstFormularConst = {
  no: {
    ln1: 0,
    ln2: 1000,
    ai1: 0,
    ai2: 27648,
  },
  dust: {
    ln1: 0,
    ln2: 100,
    ai1: 0,
    ai2: 27648,
  },
  temperature: {
    ln1: 0,
    ln2: 300,
    ai1: 0,
    ai2: 27648,
  },
  t: {
    ln1: 0,
    ln2: 300,
    ai1: 0,
    ai2: 27648,
  },
  pressure: {
    ln1: -5000,
    ln2: 5000,
    ai1: 0,
    ai2: 27648,
  },
  flow: {
    ln1: 0,
    ln2: 2000000,
    ai1: 0,
    ai2: 27648,
  },
  flow2: {
    ln1: 0,
    ln2: 2000000,
    ai1: 0,
    ai2: 27648,
  },
  h2o: {
    ln1: 0,
    ln2: 40,
    ai1: 0,
    ai2: 27648,
  },
  co: {
    ln1: 0,
    ln2: 1000,
    ai1: 0,
    ai2: 27648,
  },
  co2: {
    ln1: 0,
    ln2: 25,
    ai1: 0,
    ai2: 27648,
  }
}

const secondFormularConst = {
  //water
  so2: {
    ln3: 1000
  },
  o2: {
    ln3: 25
  },
  co21: {
    ln3: 25
  },

  //air
  cod: {
    ln3: 800
  },
  tn: {
    ln3: 50
  },
  tss: {
    ln3: 400
  },
  tp: {
    ln3: 400
  }
}

const constValue = {
  ph: 0,
  flow: 0
}

const firstFormular = (sensor, value) => {
  const { ln1, ln2, ai1, ai2 } = firstFormularConst[sensor.toLowerCase()]
  return value * ((ln2 - ln1) / (ai2 - ai1)) + ((ai2 * ln1 - ln2 * ai1) / (ai2 - ai1))

}

const secondFormular = (sensor, value) => {
  const { ln3 } = secondFormularConst[sensor.toLowerCase()]
  return (value - 400) * ln3/1600

}

const multipleWithConst = (sensor, value) => {
  return constValue[sensor.toLowerCase()] * value
}

const caculateData = (sensor, value, station) => {
  const use1 = ['no', 'dust', 'temperature', 't', 'pressure', 'flow', 'flow2', 'h2o', 'co', 'co2']
  const use2 = ['so2', 'o2', 'co21', 'cod', 'tn', 'tss', 'tp']
  const useConst = ['ph']
  if(use1.indexOf(sensor.toLowerCase()) > -1) {
    return firstFormular(sensor, value)
  }

  if(use2.indexOf(sensor.toLowerCase()) > -1) {
    return secondFormular(sensor, value)
  }
  if (useConst.indexOf(sensor.toLowerCase()) > -1) {
    return multipleWithConst(sensor, value)
  }

  return value
}

const backupFolder = path.join('E:', 'data_backup_')
// const backupFolder = path.join(__dirname, '..', 'data_backup_')

module.exports = {
  concurrency: 1,
  handler: ({
    data: {
      path: filepath
    }
  }) => {
    const data = fs.readFileSync(filepath).toString().split('\n').splice(2)
    const tasks = []
    data.forEach(row => {
      if (row !== '') {
        const item = row.split(';')
        const dt = new Data({
          name: item[0].split('.')[0],
          sensor: item[0].split('.')[1],
          timeStamp: item[1],
          value: item[2],
          quality: item[3].replace('\n', '')
        })

        dt.value = caculateData(dt.sensor, dt.value, dt.name)

        socket.emit('new-data', dt)
        tasks.push(dt.save())
      }

    })
    return Promise.all(tasks)
    .then(() => {
      const newfilepath = path.join(backupFolder, path.basename(filepath))
      return fs.renameSync(filepath, newfilepath)
    })
  }
}