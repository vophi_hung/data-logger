import mongoose from 'mongoose'
mongoose.connect('mongodb://localhost/test', {
  useMongoClient: true
})
mongoose.Promise = Promise
export default mongoose