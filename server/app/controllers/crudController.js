export default class CrudControler {
  constructor(Service) {
    this.Service = Service
  }

  async create(params, options) {
    return await this.Service.create(...arguments)
  }

  async delete(params) {
    return await this.Service.delete(...arguments)
  }
  
  async getItem(params) {
    return await this.Service.getItem(...arguments)
  }

  async getList(params, options) {
    return await this.Service.getList(...arguments)
  }

  async update(params) {
    return await this.Service.update(...arguments)
  }

}
