import CrudController from './crudController'
import Collections from '@/collections'
const { Data } = Collections

export default class brandController {

  async getByDateRange(name, start, end) {
    const condition = {
      name
    }
    
    if(start && start !== 'undefined' || end && end !== 'undefined') {
      condition['timeStamp'] = {}
      if(start) {
        condition['timeStamp'].$gte = new Date(start)
      }

      if(end) {
        condition['timeStamp'].$lte = new Date(end)
      }
    }

    const listTime = await Data.aggregate([
      {
        $match: condition,
      },{
        $group : {
           _id :  '$timeStamp'
        }
      }, {
        $sort: {
          _id: 1
        }
      }      
    ])
    
    const tasks = []
    const sensors = []
    listTime.forEach(date => {
      tasks.push(Data.find({
        name,
        timeStamp: date._id
      }).then(items => {
        let row = {
          timeStamp: date._id
        }

        items.forEach(item => {
          row[item.sensor] = item.value
          if(sensors.indexOf(item.sensor) === -1) {
            sensors.push(item.sensor)
          }
        })
        return row
      }))
    })

    return {
      sensors,
      data: await Promise.all(tasks)
    }
  }

  async getLatestData(name) {
    let latest = await Data.findOne(
      { name },
      null,
      {
        sort: {
          timeStamp: -1
        }
    })
    
    const sensors = await Data.find({
      name,
      timeStamp : latest.timeStamp
    })

    const data = {
      timeStamp: latest.timeStamp
    }

    sensors.forEach(sensor => {
      data[sensor.sensor] = sensor.value
    })

    return data
  }

  async getStartupData(name, sensor, limit=20) {
    try {
      return await Data.find({
        name,
        sensor
      })
      .sort({timeStamp: -1})
      .limit(limit)  
    } catch (error) {
      console.log(error)
    }
    
  }
  
  async getListStation(name) {
    return Data.aggregate([
      {
        $match: {
          name: {
            $regex: name, 
            $options: 'g'
          }
        }
      }, {
        $group : {
           _id :  '$name'
        }
      }
      
    ])
  }

  async getListSensor(name) {
    return Data.aggregate([
      {
        $match: {
          name
        }
      }, {
        $group : {
           _id :  '$sensor'
        }
      }      
    ])
  }

  async getList() {
    let data
    try {
      data = await Data.find()
    } catch (error) {
      console.log(error)
    }
    return data
  }

  async create(params) {
    try {
      const data = new Data(params)
      return await data.save()
                  
    } catch (error) {
      return error
    }
  }
}