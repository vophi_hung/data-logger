import bodyParser from 'body-parser'
import cors from 'cors'
import express from 'express'
import http from 'http'
import logger from 'morgan'
import api from './routers'
import socket from 'socket.io'
import {
  configService
} from './services'
let app = express()

const server = http.Server(app)

app
app.use(logger('common'))
  .use(bodyParser.json({
    limit: '50mb'
  }))
  .use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
  }))
  .use('/', [
    cors({
      origin: configService.getAllowOrigin(),
      optionsSuccessStatus: 200
    })
  ])
  .use('/api', api)
//  .use(response_helper)

const io = socket(server)

io.on('connection', function (socket) {
  socket.on('new-data', function (data) {
    const { name, sensor } = data
    socket.broadcast.emit(`${name}-${sensor}`, data)
  });
});

server.listen(configService.getServerPort(), () => {
  console.log('%s started at %s://%s:%s',
    configService.getServerName(),
    configService.getServerProtocol(),
    configService.getServerHost(),
    configService.getServerPort()
  )
})


export default app;