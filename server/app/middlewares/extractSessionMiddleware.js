import CONST from '@/const'
import BaseMiddleware from './base'
import {
  configService,
  utilService
} from '@/services'

export default class PageInfoMiddleware extends BaseMiddleware {
  use(req, _res, next) {
    const authorization = req.header('authorization').toLowerCase().replace('bearer', '').trim()
    const {
      id
    } = utilService.verifyAuthToken(authorization, configService.getSecret())
    req.auth = {
      id
    }
    next()
  }
}