import PageInforMiddleware from './pageInfoMiddleware'
import ExtractSessionMiddleware from './extractSessionMiddleware'

const pageInfoMiddleware = new PageInforMiddleware()
const extractSessionMiddleware = new ExtractSessionMiddleware()

export {
    extractSessionMiddleware,
    pageInfoMiddleware
}