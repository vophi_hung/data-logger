import config from 'config'
import Sequelize from 'sequelize'

const sequelize = new Sequelize(
  config.get('db.database'),
  config.get('db.username'),
  config.get('db.password'), {
    host: config.get('db.host'),
    dialect: config.get('db.dialect'),
    // default setting
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    }
  })

export {
  Sequelize,
  sequelize
}