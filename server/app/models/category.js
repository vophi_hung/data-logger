import CONST from '../const'
import {
  sequelize,
  Sequelize
} from './base'

export default sequelize.define('tbl_category', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  brand_id: Sequelize.STRING,
  description: Sequelize.STRING,
  image: Sequelize.STRING,
  name: Sequelize.STRING,
  parent_id: Sequelize.STRING,
  status: Sequelize.ENUM(CONST.STATUS_ENUM),
  created_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  updated_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  }
}, {
  timestamps: true,
  underscored: true,
  freezeTableName: true
})