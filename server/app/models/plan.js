import CONST from '../const'
import {
  sequelize,
  Sequelize
} from './base'

export default sequelize.define('tbl_plan', {
  id: {
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV1,
    primaryKey: true
  },
  name: Sequelize.STRING,
  description: Sequelize.STRING,
  status: Sequelize.STRING,

  // platform
  allow_dashboard: Sequelize.BOOLEAN,
  allow_website: Sequelize.BOOLEAN,
  allow_chatbot: Sequelize.BOOLEAN,
  allow_delivery_app: Sequelize.BOOLEAN,
  allow_order_tracking_app: Sequelize.BOOLEAN,

  // Third party
  allow_call_center: Sequelize.BOOLEAN,
  allow_call_center_app: Sequelize.BOOLEAN,
  allow_intergrate_ghn: Sequelize.BOOLEAN,
  allow_intergrate_haravan: Sequelize.BOOLEAN,
  number_user: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  created_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  },
  updated_at: {
    type: 'TIMESTAMP',
    defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    allowNull: false
  }
}, {
  timestamps: true,
  underscored: true,
  freezeTableName: true
})