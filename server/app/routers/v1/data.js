import express from 'express'
import BaseRouter from '../base'
import {
  dataController
} from '@/controllers'
import {
  pageInfoMiddleware
} from '@/middlewares'


export default class DataRouter extends BaseRouter {
  constructor() {
    super()
    this.router = express.Router()
    this.router.get('/startup/:name/:sensor', [pageInfoMiddleware.run()], this.route(this.getStartupData))
    this.router.get('/station/:name', [pageInfoMiddleware.run()], this.route(this.getListStation))
    this.router.get('/sensor/:name', [pageInfoMiddleware.run()], this.route(this.getListSensor))
    this.router.get('/sensor/report/:name', [pageInfoMiddleware.run()], this.route(this.getReport))
    this.router.get('/sensor/latest/:name', [pageInfoMiddleware.run()], this.route(this.getLatest))
  }

  async getStartupData(req, res) {
    const limit = parseInt(req.query.limit || 20)
    const { name, sensor } = req.params
    const items = await dataController.getStartupData(name, sensor, limit)
    this.onSuccessAsList(res, items, undefined, req.pageInfo)
  }

  async getLatest(req, res) {
    try {
      req.items = await dataController.getLatestData(req.params.name)  
    } catch (error) {
      console.log(error)    
    }
    this.onSuccess(res, req.items, undefined, req.pageInfo)
  }

  async getListSensor(req, res) {
    try {
      req.items = await dataController.getListSensor(req.params.name, req.pageInfo)          
    } catch (error) {
      console.log(error)
    }
    this.onSuccessAsList(res, req.items, undefined, req.pageInfo)
  }

  async getListStation(req, res) {
    try {
      req.items = await dataController.getListStation(req.params.name, req.pageInfo)          
    } catch (error) {
      console.log(error)
    }
    this.onSuccessAsList(res, req.items, undefined, req.pageInfo)
  }

  async getReport(req, res) {
    try {
      const { start, end } = req.query
      req.items = await dataController.getByDateRange(req.params.name, start, end)  
    } catch (error) {
      console.log(error)    
    }
    
    this.onSuccess(res, req.items, undefined, req.pageInfo)    
  }
}
