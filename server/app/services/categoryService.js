import {
  Category
} from '@/models'
import CrudService from './crudService'

export default class CategoryService extends CrudService {
  constructor() {
    super(Category)
  }
}