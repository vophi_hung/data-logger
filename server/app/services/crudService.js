export default class CrudControler {
  constructor(Model) {
    this.Model = Model
  }
 
  async create(params) {
    return await this.Model.create(params)
  }

  async delete(params) {
    const {
      id
    } = params

    const item = await this.Model.findById(id)

    return await item.destroy(params)
  }

  async getItem(params) {
    return await this.Model.findOne({
      where: params
    })
  }

  async getList(params = {}, options = {limit: 10, offset: 0}) {
    params = Object.assign(params, options.filter)
    return await this.Model.findAndCountAll({
      where: params,
      limit: options.limit,
      offset: options.offset
    })
  }

  async update(params) {
    const {
      id
    } = params

    const item = await this.Model.findById(id)
    await item.update(params)
    return this.getItem({ id })
  }

}
