import {
  Data
} from '@/collections'
import CrudService from './crudService'

export default class BrandService extends CrudService {
  constructor() {
    super(Data)
  }

  async getListSensor(name) {
    return Data.aggrigate([
      {
        $match: {
          name
        }
      }, {
        $group : {
           _id : { name }
        }
      }
      
    ])
  }
}