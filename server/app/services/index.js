import DataService from './dataService'
import ConfigService from './configService'
import ErrorService from './errorService'
import UtilService from './utilService'

//Singleton
const configService = new ConfigService()
const dataService = new DataService()
const errorService = new ErrorService()
const utilService = new UtilService()

export {
  dataService,
  configService,
  errorService,
  utilService
}