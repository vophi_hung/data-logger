export default class UtilService {
  stringifyQuery(query) {
    query = query || {}
    query = Object.keys(query).reduce((acc, key) => {
      if (query[key] !== undefined && query[key] !== null) {
        acc.push(`${key}=${encodeURIComponent(typeof query[key] === 'object' ? JSON.stringify(query[key]) : query[key])}`)
      }
      return acc
    }, [])
    return query.join('&')
  }
}