const db = require('./database')

module.exports = {
  db: db['development'],
  server: {
    port: 3000,
    name: 'INNOWAY API Server',
    protocol: 'http',
    host: 'localhost'
  }
}