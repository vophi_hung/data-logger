var gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  babel = require('gulp-babel'),
  Cache = require('gulp-file-cache'),
  minify = require('gulp-minify')

var cache = new Cache();

gulp.task('compile', function () {
  var stream = gulp.src('./app/**/*.js') // your ES2015 code
    .pipe(cache.filter()) // remember files
    .pipe(babel({
      presets: ['es2015']
    })) // compile new one
    .pipe(cache.cache()) // cache them
    .pipe(gulp.dest('./dist')) // write them
  return stream // important for gulp-nodemon to wait for completion
})

gulp.task('start', ['compile'], function () {
  var stream = nodemon({
    script: 'dist/', // run ES5 code
    watch: 'app', // watch ES2015 code
    tasks: ['compile'] // compile synchronously onChange
  })

  return stream
})

gulp.task('compress', function () {
  gulp.src('dist/**/*.js')
    .pipe(minify({
      ext: {
        src: '-debug.js',
        min: '.js'
      },
      noSource: true
    }))
    .pipe(gulp.dest('build'))
});