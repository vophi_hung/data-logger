import Utils from '../test-utils'

const {
  chaiApp,
  should
} = Utils

describe('Manage brand', () => {
  describe('Get list brand', () => {
    it('Get list brand successfully', async () => {
      const res = await chaiApp.get('/api/v1/category')
      const { code, result } = res.body
      
      code.should.equal(200)
    })

    it('Get list item successfully', async () => {
      const res = await chaiApp.get('/api/v1/category/4e8eeef0-4dbc-11e7-8630-95073a62728b')
      const { code, result } = res.body
      
      code.should.equal(200)
    })
  })
})