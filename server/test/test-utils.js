import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../app'

chai.use(chaiHttp)

const chaiApp = chai.request(server)
export default {
  chaiApp,
  should: chai.should()
}