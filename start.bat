call pm2 list
call cd server
call pm2 start dist --name server
call cd ..
call pm2 start client/server.js --name client
call pm2 start queue
call pm2 start watcher