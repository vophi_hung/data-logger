const Queue = require('bull')
const chokidar = require('chokidar')
const path = require('path')

var watcher = chokidar.watch(path.join('E:', 'Data'), {
// var watcher = chokidar.watch(path.join(__dirname, '..', 'files'), {
  ignored: /(^|[\/\\])\../,
  persistent: true,
  ignoreInitial: true,
});
var log = console.log.bind(console);
watcher
.on('add', path => {
  let kue = new Queue('saveFileToDB', {
    host: 'localhost',
    port: 6379
  })
  kue.add({
    path
  })
})

var http = require('http');
var fs = require('fs');

http.createServer(function (req, res) {
  console.log('watcher server listen on port 9615')
}).listen(9615);